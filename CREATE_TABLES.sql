/*
CREATE TABLE USER_ACCOUNTS (
	UserName		VARCHAR(25) NOT NULL,
    DOB			 	DATE NOT NULL,
    BioSex			VARCHAR(1) NOT NULL,
    Daily_Goal		INT DEFAULT 1500,
    Weight_KG		DECIMAL(6,2) NOT NULL,
    Height_CM		DECIMAL(6,2) NOT NULL,
    
    CONSTRAINT USER_ACCOUNTS_PK PRIMARY KEY (UserName)
    );

CREATE TABLE FOOD_BANK (
	Food_Name			VARCHAR(60) NOT NULL,
    Calories    		INT NOT NULL,
	Total_Fat   		DECIMAL(6,2),
	Sat_Fat     		DECIMAL(6,2),
	Cholesterol     	DECIMAL(6,2),
	Total_Carb     		DECIMAL(6,2),
	Dietary_Fiber   	DECIMAL(6,2),
	Sugars    			DECIMAL(6,2),
	Protein     		DECIMAL(6,2),
	Vit_A     			DECIMAL(6,2),
	Vit_C     			DECIMAL(6,2),
	Calcium     		DECIMAL(6,2),
	Iron     			DECIMAL(6,2),
	Potassium     		DECIMAL(6,2),
	Sodium     			DECIMAL(6,2),

    CONSTRAINT FOODS_PK PRIMARY KEY (Food_Name)
	);
    
CREATE TABLE MEALS (
		Meal_Entry		INT	AUTO_INCREMENT,
		UserName		VARCHAR(25),
        Date_Logged		DATE,
        Meal_Group		VARCHAR(9),
        Food_Name		VARCHAR(30),
	CONSTRAINT			MEALS_PK		PRIMARY KEY(Meal_Entry),
	CONSTRAINT			MEALS_USER_FK	FOREIGN KEY(UserName)
									REFERENCES USER_ACCOUNTS(UserName)
											ON UPDATE CASCADE,
	CONSTRAINT 			MEALS_FOOD_FK	FOREIGN KEY (Food_Name)
								REFERENCES	FOOD_BANK (Food_Name)
									ON UPDATE CASCADE
	);

CREATE TABLE RDV (
	RDV_Description		VARCHAR(30) NOT NULL,
    Calcium     		DECIMAL(6,2),
    Cholesterol     	DECIMAL(6,2),
	Dietary_Fiber   	DECIMAL(6,2),
	Iron     			DECIMAL(6,2),
	Potassium     		DECIMAL(6,2),
	Sodium     			DECIMAL(6,2),
    Vit_A     			DECIMAL(6,2),
	Vit_C     			DECIMAL(6,2),
	Sugars    			DECIMAL(6,2),
    CONSTRAINT RDV_PK PRIMARY KEY (RDV_Description)
    );
    
CREATE TABLE EXERCISE_BANK (
	Exercise_Name		VARCHAR(30) NOT NULL,
    Calories_57			INT,	/* Number of KCAL burned at a specific weight 
    Calories_70			INT,
    Calories_84			INT,
		CONSTRAINT EX_PK 	PRIMARY KEY (Exercise_Name)
    );
    
CREATE TABLE EXERCISE_LOG (
	Log_ID				INT AUTO_INCREMENT,
    UserName			VARCHAR(25) NOT NULL,
    Exercise_Name		VARCHAR(30) NOT NULL,
    Date_Recorded		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    Duration_Minutes	INT,
    Weight_KG			DECIMAL(6,2), /* Must be entered at time of logging for each entry. 
		CONSTRAINT EX_LOG_PK	PRIMARY KEY (Log_ID),
        CONSTRAINT EX_LOG_FK	FOREIGN KEY (Exercise_Name)
								REFERENCES	EXERCISE_BANK (Exercise_Name)
    );  /* Calories burned is per 30 minutes of exercise */
    
    ALTER TABLE EXERCISE_LOG 
		ADD CONSTRAINT EX_LOG_UN	FOREIGN KEY (UserName)
			REFERENCES USER_ACCOUNTS (UserName)
				ON UPDATE CASCADE;