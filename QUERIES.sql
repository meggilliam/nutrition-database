/* HOW MANY CALORIES DID BRUENOR CONSUME ON 11/10/19?

CREATE VIEW BRUENOR_TOTALS
	AS SELECT MEALS.UserName, MEALS.Date_Logged, SUM(FOOD_BANK.Calories)
		FROM MEALS, FOOD_BANK
			WHERE MEALS.USERNAME = 'BRUENOR'
				AND MEALS.Date_Logged = '2019-11-10'
                AND MEALS.Food_Name = FOOD_BANK.Food_Name;

SELECT * FROM BRUENOR_TOTALS					
*/
/* What foods have Cattie recorded for her lunches?

SELECT * FROM MEALS
	WHERE MEALS.UserName = "CATTIE"
		AND MEALS.Meal_Group = "Lunch"
    ORDER BY MEALS.Date_Logged;
*/
/* What is the lowest Calorie food in the database?*/
SELECT Food_Name, MIN(Calories)
	FROM FOOD_BANK;  

/* What foods in the Database have not been recorded in anyone's meals?
SELECT Food_Name FROM FOOD_BANK LEFT JOIN MEALS
	USING (Food_Name)
    WHERE MEALS.Food_Name IS NULL;
*/     
/* Which users recorded both food and a workout on 11/10/19?
SELECT DISTINCT UserName
	FROM MEALS
		INNER JOIN EXERCISE_LOG USING (UserName);
*/
/* Create Age Function 
DELIMITER //
CREATE FUNCTION age_calculate (date1 date) RETURNS INT DETERMINISTIC
	BEGIN
		DECLARE today DATE;
		SELECT current_date()INTO today;
		RETURN year(today)-year(date1);
	END;
// DELIMITER ;*/
/* Create TDEE View
UPDATE USER_ACCOUNTS
	SET Daily_Goal = 2033
    WHERE USER_ACCOUNTS.UserName = "BRUENOR";
SELECT * FROM USER_ACCOUNTS WHERE UserName = 'BRUENOR';

DROP VIEW TDEE_CALC;
CREATE VIEW TDEE_CALC AS
	SELECT USER_ACCOUNTS.UserName, USER_ACCOUNTS.Daily_Goal, 
		(age_calculate(USER_ACCOUNTS.DOB)) AS Age, 
		((10*USER_ACCOUNTS.Weight_KG)+(6.25*USER_ACCOUNTS.Height_CM)-((5*age_calculate(USER_ACCOUNTS.DOB))+5)) 
        AS Calculated_TDEE
    FROM USER_ACCOUNTS
    WHERE USER_ACCOUNTS.UserName = "BRUENOR";

SELECT * FROM TDEE_CALC
 */
/* Create Daily Report Procedure 
 DELIMITER //
DROP PROCEDURE Daily_Report;
CREATE PROCEDURE Daily_Report(IN temp_username VARCHAR(25), IN temp_date DATE)
BEGIN
SELECT * FROM MEALS
	WHERE MEALS.UserName = temp_username
		AND MEALS.Date_Logged = temp_date
    ORDER BY MEALS.Meal_Group;
END
// DELIMITER ;
CALL Daily_Report("Bane", '2019-11-10')*/
/* Create Calorie Burn Function  
DELIMITER //
CREATE FUNCTION calorie_burn (minutes INT, calories_per_30 INT) RETURNS DECIMAL(6,2) DETERMINISTIC
	BEGIN
		DECLARE calories_per_minute DECIMAL(6,2);
		SELECT ((calories_per_30)/30) INTO calories_per_minute;
		RETURN (minutes) * (calories_per_minute);
	END;
// DELIMITER ;*/
/* Test calorie_burn Function 
SELECT calorie_burn(60, 100) AS KCal_Burned
*/
/* Query that uses subquery - Find out Chauntea's Lunch on 11/10, 
then pull calories -- Should be Flounder, for 100 

SELECT Food_Name, Calories FROM FOOD_BANK
	WHERE Food_Name IN (
		SELECT Food_Name FROM MEALS
			WHERE Meal_Group = 'Lunch' AND 
				UserName = 'CHAUNTEA' AND
                Date_Logged = '2019-11-10');*/
/* Query that uses UNION - Create System Use Log by merging Meals and Exercise Log 
SELECT DISTINCT UserName, Date_Logged FROM MEALS
UNION DISTINCT
SELECT DISTINCT Username, Date_Recorded FROM EXERCISE_LOG;            */
/* Which nutrients did user CATTIE not meet the FDA’s recommended daily intake amounts 11/11/19?  
***Visual compare only. Abandoned tack. 
DROP VIEW CATTIE_NUTRIENTS CASCADE;
CREATE VIEW CATTIE_NUTRIENTS
	AS SELECT MEALS.UserName, SUM(FOOD_BANK.Calcium) AS Calcium_Total, 
		SUM(FOOD_BANK.Cholesterol) AS Cholesterol_Total, 
		SUM(FOOD_BANK.Dietary_Fiber) AS Fiber_Total, SUM(FOOD_BANK.Iron) AS Iron_Total,
		SUM(FOOD_BANK.Potassium) AS Potassium_Total, SUM(FOOD_BANK.Sodium) AS Sodium_Total, 
        SUM(FOOD_BANK.Vit_A) AS Vit_A_Total, SUM(FOOD_BANK.Vit_C) AS Vit_C_Total,
        SUM(FOOD_BANK.Sugars) AS Sugars_Total
		FROM MEALS, FOOD_BANK
			WHERE MEALS.USERNAME = 'CATTIE'
				AND MEALS.Date_Logged = '2019-11-11'
				AND MEALS.Food_Name = FOOD_BANK.Food_Name
		UNION 
        SELECT * FROM RDV WHERE RDV_Description = 'RDV_Minimum';

SELECT * FROM CATTIE_NUTRIENTS;*/
/* Count male and female users 
SELECT BioSex, COUNT(UserName) FROM USER_ACCOUNTS GROUP BY BioSex;
*/
/* Exercise logs 45min or longer	
SELECT UserName, Exercise_Name, Date_Recorded FROM EXERCISE_LOG WHERE Duration_Minutes >= 45
*/
