# Nutrition Database

As the final project in Database Management, Fall 2019, I created a MySQL database that would serve as the back-end of an app like MyFitnessPal for nutrition tracking. 

Data Sources:
 - Exercises and Calorie Burn Values: https://www.health.harvard.edu/diet-and-weight-loss/calories-burned-in-30-minutes-of-leisure-and-routine-activities
 - Foods and Nutritional Values: https://data.world/adamhelsinger/food-nutrition-information
 - RDV Allowances: https://en.wikipedia.org/wiki/Dietary_Reference_Intake#Current_recommendations_for_United_States_and_Canada
 - Usernames are characters and the pantheon of the fictional world Faerûn.
 - Dates of Birth were generated using https://www.randomlists.com/
 - Weights and Heights were generated using https://www.random.org/